###
# SoundSpess BYOND Interfaces
#
# Copyright (c)2019-2020 Rob "N3X15" Nelson and SoundSpess Contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
###

# Suppress errors.
window.onerror = ->
  return true

# Initialize the client's soundspace.
soundSpace = new SoundSpess()

ssbyapiNewSource = (jsonData) ->
  soundSpace.createSource JSON.parse(jsonData)
  return true

ssbyapiPlayOnce = (jsonData) ->
  soundSpace.playOnce JSON.parse(jsonData)
  return true

ssbyapiRemoveSource = (ssid) ->
  soundSpace.removeSource ssid
  return true

ssbyapiPauseSource = (ssid) ->
  soundSpace.pauseSource ssid
  return true

ssbyapiPlaySource = (ssid) ->
  soundSpace.playSource ssid
  return true

ssbyapiChangeSourceAudio = (ssid, src) ->
  soundSpace.changeSourceAudio ssid, src
  return true

ssbyapiClear = () ->
  soundSpace.clear()
  return true

ssbyapiChangeSourceLocation = (jsonData) ->
  [ssid, aID, x, y, z, vx, vy, vz] = JSON.parse jsonData
  position = [x, y, z]
  velocity = [vx, vy, vz]
  soundSpace.updateSource ssid, aID, position, velocity

ssbyapiUpdateListener = (jsonData) ->
  [aID, x, y, z, vx, vy, vz] = JSON.parse jsonData
  position = [x, y, z]
  velocity = [vx, vy, vz]
  soundSpace.updateListener aID, position, velocity
