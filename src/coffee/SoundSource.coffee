###
# SoundSpess Sound Source
#
# Copyright (c)2019-2020 Rob "N3X15" Nelson and SoundSpess Contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
###

class SoundSource
  @MUFFLE_FREQ_CUTOFF: 200 # idfk
  constructor: (@soundspace, data=null) ->
    # Unique ID for this source. Assigned server-side.
    @ID = 0
    # URL of the sound data. Used in Audio element.
    @URL = ''
    # GainNode value.
    @Gain = 1
    # Used for muffle calcs.
    @AreaID = ''
    # Position of the sound RELATIVE TO THE SOUNDSPACE. @Z is just height offset.
    @X = 0
    @Y = 0
    @Z = 0
    # Orientation of the source.
    @OrientationX = 0
    @OrientationY = 0
    @OrientationZ = -1
    # Is this sound positional? Switches PannerNode in and out.
    @Positional = true
    # Don't fuck with these unless you know what you're doing.
    @PanModel = 'HRTF'
    @DistanceModel = 'linear'
    @RefDistance = 1
    @MaxDistance = 10000
    @RollOff = 10
    @ConeInnerAngle = 360
    @ConeOuterAngle = 360
    @ConeOuterGain = 1

    @LastPositionX = 0
    @LastPositionY = 0
    @LastPositionZ = 0

    @Vary = false
    @MinRate = 0.9
    @MaxRate = 1.1
    @CurRate = 1.0

    @StartedAt = 0 # deciseconds since playSource() was called server-side.

    @Loop = false
    @Length = 0 # TTL in deciseconds

    @audio = null
    @track = null
    @gainNode = null
    @muffleFilter = null
    @panNode = null

    if data
      @deserialize data

  deserialize: (data) ->
    [
      @ID,
      @URL,
      @Gain,
      @AreaID,
      @X,@Y,@Z,
      @OrientationX,
      @OrientationY,
      @OrientationZ,
      @Positional,
      @PanModel,
      @DistanceModel,
      @RefDistance,
      @MaxDistance,
      @RollOff,
      @ConeInnerAngle,
      @ConeOuterAngle,
      @ConeOuterGain,
      @Vary,
      @MinRate,
      @MaxRate,
      @StartedAt,
      @Loop,
      @Length
    ] = data

    # Just used so we can set coordinates independently.
    @LastPositionX = @X
    @LastPositionY = @Y
    @LastPositionZ = @Z

    # Bools are represented by TRUE=1, FALSE=0 in byond.
    @Positional = @Positional == 1
    @Vary = @Vary == 1
    @Loop = @Loop == 1
    return

  initialize: ->
    # Create our <audio> tag.
    @audio = @soundspace.createAudioTag @URL

    # Set up our mediaElement.
    @track = @soundspace.ctx.createMediaElementSource @audio

    # For sounds that vary in tune a bit.
    if @Vary
      @CurRate = Math.random() * (@MaxRate-@MinRate)+@MinRate
    else
      @CurRate = @MinRate
    @CurRate = @CurRate.clamp 0, 1
    @track.playbackRate.value = @CurRate

    # Volume control
    @gainNode = new GainNode
    @gainNode.gain.value = @Gain

    # Muffling effect (lowpass filter)
    @muffleFilter = @soundspace.ctx.createBiquadFilter()
    @muffleFilter.type = 'allpass'
    @muffleFilter.frequency.value = @MUFFLE_FREQ_CUTOFF

    if @Positional
      @panNode = new PannerNode @soundspace.ctx,
        panningModel:   @PanModel,
        distanceModel:  @DistanceModel
        positionX:      @X,
        positionY:      @Y,
        positionZ:      @Z,
        orientationX:   @OrientationX,
        orientationY:   @OrientationY,
        orientationZ:   @OrientationZ,
        refDistance:    @RefDistance,
        maxDistance:    @MaxDistance,
        rolloffFactor:  @RollOff,
        coneInnerAngle: @ConeInnerAngle,
        coneOuterAngle: @ConeOuterAngle,
        coneOuterGain:  @ConeOuterGain
    # Now we snap everything together.
    # MESource(<audio>) -> GainNode [-> PannerNode] -> Your ears
    node = @track

    # Modify gain (always added so we can fuck with it later)
    node = node.connect @gainNode

    # Add positional element
    if @Positional
      node = node.connect @panNode

    node = node.connect @muffleFilter

    node.connect @soundspace.ctx.destination
    return

  setMuffled: (enabled) ->
    @muffleFilter.type = if enabled then "lowpass" else "allpass"
    return

  remove: ->
    @audio.pause()
    return

  play: ->
    @audio.play()
    return

  pause: ->
    @audio.pause()
    return

  changeAudio: (src) ->
    @audio.src = src
    return

  update: (area, position, velocity) ->
    @AreaID = area
    @setPosition position[0], position[1], position[2]
    return

  setPosition: (x, y, z=0) ->
    @setPositionX x
    @setPositionY y
    @setPositionZ z
    return

  setGain: (v) ->
    @gainNode.gain.value = v
    return

  setPositionX: (x) ->
    if @panNode.positionX
      @panNode.positionX.setValueAtTime x, @soundspace.ctx.currentTime
    else
      @panNode.setPosition x, @LastPositionY, @LastPositionZ
    @LastPositionX = x
    return

  setPositionY: (y) ->
    if @panNode.positionY
      @panNode.positionY.setValueAtTime y, @soundspace.ctx.currentTime
    else
      @panNode.setPosition @LastPositionX, y, @LastPositionZ
    @LastPositionY = y
    return

  setPositionZ: (z) ->
    if @panNode.positionZ
      @panNode.positionZ.setValueAtTime z, @soundspace.ctx.currentTime
    else
      @panNode.setPosition @LastPositionX, @LastPositionY, z
    @LastPositionZ = z
    return
