###
# SoundSpess Sound System
#
# Copyright (c)2019-2020 Rob "N3X15" Nelson and SoundSpess Contributors
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
###

# Note: Represents a single z-level.
class SoundSpess
  @WINDOW: window
  @AUDIOCONTEXT: @WINDOW.AudioContext or @WINDOW.webkitAudioContext
  @AUDIO: @WINDOW.Audio

  constructor: ->
    @ctx = new @AUDIOCONTEXT
    @listener = @ctx.listener
    @areaID = null

    # Organized by ID
    @sources = {}

    # Stuff attached to the player ("global" sounds)
    @attached = []

    # Initial position while we wait for a server update.
    @listener.positionX.value = 0
    @listener.positionY.value = 0
    @listener.positionZ.value = 0

    # If paused due to autoplay restrictions, resume.
    @ctx.resume()

  recalculateMuffle: ->
    for id, source in @sources
      source.setMuffled source.areaID != @areaID
    return

  createAudioTag: (src) ->
    return new @AUDIO src

  createSource: (data) ->
    ss = @sources[id] = new SoundSource @, data
    return ss

  playOnce: (data) ->
    ss = @createSource data
    ss.audio.attachEventListener 'ended', (e) =>
      @remove ss.id
      return
    return

  removeSource: (id) ->
    if id of @sources
      @sources[id].remove()
      delete @sources[id]
    return

  playSource: (id) ->
    if id of @sources
      @sources[id].play()
    return

  pauseSource: (id) ->
    if id of @sources
      @sources[id].pause()
    return

  updateSource: (id, areaID, position, velocity) ->
    if id of @sources
      @sources[id].update areaID, position, velocity
    return

  changeSourceAudio: (id, src) ->
    if id of @sources
      @sources[id].changeAudio src
    return

  clear: ->
    for ssid of @sources
      @removeSource ssid
    @sources = {}
    @attached = []
    return

  updateListener: (area, pos, vel) ->
    @areaID = area
    [x, y, z] = pos
    [vx, vy, vz] = vel
    @listener.positionX.value = x
    @listener.positionY.value = y
    @listener.positionZ.value = z

    # Velocity not implemented in SS13?

    for src in @attached
      src.update @areaID, pos, vel

    @recalculateMuffle()
    return
