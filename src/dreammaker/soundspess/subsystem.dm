
var/datum/subsystem/sound/SSsound

/datum/subsystem/sound
	name = "SoundSpess Positional Audio System"
	init_order    = SS_INIT_SOUND
	display_order = SS_DISPLAY_SOUND
	priority      = SS_PRIORITY_SOUND
	wait          = 5 SECONDS

	var/list/datum/soundlistener/listeners

	var/next_sound_id = 0

/datum/subsystem/sound/New()
	NEW_SS_GLOBAL(SSsound)

/datum/subsystem/sound/stat_entry()
	..("Z:[zlevels.len]|S:[sources.len]")

/datum/subsystem/sound/Initialize(timeofday)
	..()

/datum/subsystem/sound/createSource(url, var/area/a, x, y, z, z_offset=0, gain=1, vary_rate=TRUE, min_rate=SOUNDSPESS_MIN_RATE, max_rate=SOUNDSPESS_MAX_RATE)
	if (issound(url))
		url = sound2base64(url)

	// No not that SS. Calm down, hitler
	var/datum/soundsource/ss = new /datum/soundsource()
	ss.id = src.next_sound_id++
	ss.url = url
	ss.area = a
	ss.x = x
	ss.y = y
	ss._zlevel = z
	ss.z = z_offset
	ss.min_rate = min_rate
	ss.max_rate = max_rate
	ss.vary = vary_rate
	return ss

/datum/subsystem/sound/playOnceAt(url, var/turf/T)
	var/area/a = get_area(T)
	var/datum/soundsource/ss = src.createSource(url, a, T.x, T.y, T.z)
	for(var/datum/soundlistener/listener in listeners)
		if(ss._zlevel == T.z)
			listener.playOnce(ss)
	return ss

/datum/subsystem/sound/playSourceOnceAt(var/datum/soundsource/ss, var/turf/T)
	var/area/a = get_area(T)
	for(var/datum/soundlistener/listener in listeners)
		if(ss._zlevel == T.z)
			listener.playOnce(ss)

/datum/subsystem/sound/addSourceAt(var/datum/soundsource/ss, var/turf/T)
	var/area/a = get_area(T)
	for(var/datum/soundlistener/listener in listeners)
		if(ss._zlevel == T.z)
			listener.addSource(ss)
