// Rust bindings and shit.

// Make the library marshalling less of a clusterfuck.  Yes, I stole this from PJB.  Sue me. It works great.
#define LIBSOUNDSPESS call("./libsoundspess.[world.system_type == UNIX ? "so" : "dll"]", function)(arguments)

// Get the duration of a media file, in deciseconds, given a filename.
// Errors:
//  -1: File not found
//  -2: Decoding error
/proc/get_sound_duration(var/filename)
	return text2num(LIBSOUNDSPESS("get_sound_duration", filename))

#define LIBSOUND_ERR_FILE_NOT_FOUND -1
#define LIBSOUND_ERR_DECODING       -2
