/**
 * BYOND manifestation of our audiosource shit.
 *
 * This is just serialized and fired over to the clientside JS, so don't expect anything amazing.
 *
 * This is very low-level shit, so don't fuck with the variables unless you know what you are doing.
 */
/datum/soundsource
	// URI of the sound data.  datafied sounds *might* work. No idea how to get RSC'd stuff.
	// This gets stuffed into the src of an Audio element. (<audio>)
	var/url = ""

	// "Volume" of the sound.  Used in a GainNode attached to the PannerNode.
	// Value is 0-1, inclusive.
	var/gain = 1.0

	// Area the sound occurred in.  Sound will be muffled if you are not in the same area.
	// Will be serialized to a string, clientside.
	var/area/area = null

	// Coordinates of the sound on the z-level.
	var/x = 0
	var/y = 0

	// Height of the sound. ***IS NOT THE FUCKING Z-LEVEL.***
	// This is for shit like things over you (NINJA) or things under you (like that asshole banging around in trash pipes)
	// Z-level is determined by which soundspace this is added to. Each z-level has its own soundspace.
	var/z = 0

	// Only used in internal tracking.
	var/_zlevel = 0

	// Switches the PannerNode on and off as needed.
	var/positional = TRUE

	// PannerNode options and tweaks.  You should not need to fuck with these.

	// PannerNode.panningModel
	var/pan_model = "HRTF"
	// PannerNode.distanceModel
	var/distance_model = "linear"
	// PannerNode.orientation{X,Y,Z}
	var/list/orientation = [0, 0, -1]
	// PannerNode.refDistance
	var/ref_distance = 1
	// PannerNode.maxDistance
	// Maximum distance from which this can be heard.  IMPORTANT!
	var/max_distance = 10000
	// PannerNode.rollOff
	// Affects how quickly the audio fades with distance. IMPORTANT!
	var/roll_off = 10

	// These three arguments affect the shape of the cone the sound is emitted in.

	// PannerNode.coneInnerAngle
	var/cone_inner_cone = 360
	// PannerNode.coneOuterAngle
	var/cone_outer_cone = 360
	// PannerNode.cone
	var/cone_outer_gain = 1

	var/vary = TRUE
	var/min_rate = 0.7 // 32000/45000
	var/max_rate = 1.2 // 55000/45000

	// world.realtime
	var/started_at=0
	var/paused_at=0

	var/loop = 0
	var/duration = 0 // For play-once, sort of a TTL in 1/10 seconds.


/datum/soundsource/proc/serialize()
	return list(
		id,
		url,
		gain,
		src.area.name,
		x,y,z,
		orientation[0],
		orientation[1],
		orientation[2],
		positional,
		pan_model,
		distance_model,
		ref_distance,
		max_distance,
		roll_off,
		cone_inner_cone,
		cone_outer_cone,
		cone_outer_gain,
		vary,
		min_rate,
		max_rate,
		started_at,
		loop,
		duration
	)

/datum/soundsource/proc/inSoundSpaceOf(var/turf/listenT)
	// If source's turf is null, assume it's in our soundspace.
	// This will mean EVERYONE can hear this, no matter the z-level.
	if(!listenT)
		return TRUE

	return src._zlevel == listenT.z

// Stop play without resumability
/datum/soundsource/proc/stop()
	paused_at = 0
	started_at = 0
	for(var/datum/soundlistener/listener in listeners)
		if (inSoundSpaceOf(listener.current_loc))
			listener.pauseSource(src.id)
		else
			listener.removeSource(src.id)

// Allows resuming via resume()
/datum/soundsource/proc/pause()
	paused_at = world.realtime - started_at
	started_at = 0
	for(var/datum/soundlistener/listener in listeners)
		if (inSoundSpaceOf(listener.current_loc))
			listener.pauseSource(src.id)
		else
			listener.removeSource(src.id)

/datum/soundsource/proc/resume()
	started_at = paused_at
	paused_at = 0
	for(var/datum/soundlistener/listener in listeners)
		if (inSoundSpaceOf(listener.current_loc))
			listener.playSource(src.id)
		else
			listener.removeSource(src.id)

/datum/soundsource/proc/play()
	started_at = world.realtime
	paused_at = 0
	for(var/datum/soundlistener/listener in listeners)
		if (inSoundSpaceOf(listener.current_loc))
			listener.playSource(src.id)
		else
			listener.removeSource(src.id)

/datum/soundsource/proc/remove()
	for(var/datum/soundlistener/listener in listeners)
		listener.removeSource(src.id)

/datum/soundsource/proc/setLoc(var/turf/T)
	for(var/datum/soundlistener/listener in listeners)
		if (inSoundSpaceOf(listener.current_loc))
			listener.updateSourceLoc(src.id, T)
		else
			listener.removeSource(src.id)
