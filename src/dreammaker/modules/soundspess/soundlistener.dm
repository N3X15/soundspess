/**
 * SoundSpess Sound Player
 * This is analogous to the old /datum/media_player.
 * !!!Be aware that this is all fairly low-level shit and you should be interacting with SSsound and /datum/soundsource instead.
 */

#ifdef DEBUG_MEDIAPLAYER
to_chat(#define MP_DEBUG(x) owner, x)
#warn Please comment out #define DEBUG_MEDIAPLAYER before committing.
#else
#define MP_DEBUG(x)
#endif

/datum/soundlistener
	var/atom/movable/holder
	var/client/owner
	var/turf/current_loc = null
	var/const/window = "rpane.hosttracker"

/datum/soundlistener/New(var/client/C, var/atom/movable/holder=null)
	src.holder=holder
	owner=C
	if(holder)
		current_loc = get_turf(holder)

// Actually pop open the player in the background.
/datum/soundlistener/proc/open()
	owner << browse(null, "window=[window]")
	owner << browse('html/soundspess.html', "window=[window]")

	// Clear any pre-existing sounds
	clear()

	// Immediately sync position in the global soundspace in the player.
	update()

// Tell soundspess to play a sound once. (removes it after it finishes playing)
/datum/soundlistener/proc/playOnce(var/datum/soundsource/ss)
	MP_DEBUG("<span class='good'>Sending SS#[ss.id] to SoundSpess (PlayOnce)...</span>")
	owner << output(list2params(list(json_encode(ss.serialize()))), "[window]:ssbyapiPlayOnce")

// Tell soundspess to add a new looping sound source.
/datum/soundlistener/proc/addSource(var/datum/soundsource/ss)
	MP_DEBUG("<span class='good'>Sending SS#[ss.id] to SoundSpess (NewSource)...</span>")
	owner << output(list2params(list(json_encode(ss.serialize()))), "[window]:ssbyapiNewSource")

// Tell soundspess to remove a sound source.
/datum/soundlistener/proc/removeSource(var/ssid)
	MP_DEBUG("<span class='good'>Removing SS#[ssid] from SoundSpess (RemoveSource)...</span>")
	owner << output(list2params(list(ssid)), "[window]:ssbyapiRemoveSource")

// Tell soundspess to pause a sound source.
/datum/soundlistener/proc/pauseSource(var/ssid)
	MP_DEBUG("<span class='good'>Pausing SS#[ssid] in SoundSpess (Pause)...</span>")
	owner << output(list2params(list(ssid)), "[window]:ssbyapiPauseSource")

// Tell soundspess to resume a sound source.
/datum/soundlistener/proc/playSource(var/ssid)
	MP_DEBUG("<span class='good'>Resume SS#[ssid] in SoundSpess (Resume)...</span>")
	owner << output(list2params(list(ssid)), "[window]:ssbyapiResumeSource")

// Tell soundspess to resume a sound source.
/datum/soundlistener/proc/clear()
	MP_DEBUG("<span class='good'>CLEAR SoundSpess!</span>")
	owner << output(list2params(list()), "[window]:ssbyapiClear")

// Tell soundspess to change the src of the Audio tag.
/datum/soundlistener/proc/changeSourceAudio(var/datum/soundsource/ss)
	MP_DEBUG("<span class='good'>Update SS#[ss.id] Audio in SoundSpess (UpdateSource)...</span>")
	owner << output(list2params(list(ss.id, ss.url)), "[window]:ssbyapiChangeSourceAudio")

// Tell soundspess to update a sound source's location.
/datum/soundlistener/proc/changeSourceLoc(var/datum/soundsource/ss, var/aname, var/x, var/y, var/z_o)
	MP_DEBUG("<span class='good'>Update SS#[ss.id] in SoundSpess (UpdateSource)...</span>")
	// TODO: Velocity
	var/v_x = 0
	var/v_y = 0
	var/v_z = 0
	owner << output(list2params(list(ss.id, aname, x, y, z_o, v_x, v_y, v_z)), "[window]:ssbyapiChangeSourceLocation")

// Update listener's location within the soundspace.
/datum/soundlistener/proc/update()
	var/turf/T = get_turf(src.holder)
	current_loc = get_turf(holder)

	if(T.z != src.lastZ)
		src.clear()

	// TODO: Velocity. Yes, we can do doppler shift.
	var/list/packet = list(get_area(T).name, T.x, T.y, 0, 0, 0, 0)
	MP_DEBUG("<span class='good'>Update Listener#[ss.id] in SoundSpess (UpdateListener)...</span>")
	owner << output(list2params(list(json_encode(packet))), "[window]:ssbyapiUpdateListener")
