//Precaching a bunch of shit
/var/savefile/soundCache = new /savefile("data/soundCache.sav") //Cache of sounds for soundspess

// Identical to icon2base64.  Yes, it works. (Tested 9/16/2019)
/proc/sound2base64(var/sound/sound, var/soundKey = "misc")
	if (!istype(sound, /sound))
		return 0

	soundCache[soundKey] << sound
	var/soundData = soundCache.ExportText(soundKey)
	var/list/partial = splittext(soundData, "{")
	return replacetext(copytext(partial[2], 3, -5), "\n", "")
