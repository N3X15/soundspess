/**
 * SoundSpess Controller Datum
 *
 * Not strictly necessary, but having it like this means it's easy to switch to deferred processing or send/receive queues, if needed later.
 */
var/datum/subsystem/sound/SSsound

/datum/subsystem/sound
	name = "SoundSpess"
	init_order    = SS_INIT_SOUND
	display_order = SS_DISPLAY_SOUND
	priority      = SS_PRIORITY_SOUND
	wait          = 5 SECONDS

	var/list/datum/soundlistener/listeners
	var/list/datum/soundsource/sounds

	var/next_sound_id = 0

/datum/subsystem/sound/New()
	NEW_SS_GLOBAL(SSsound)

/datum/subsystem/sound/stat_entry()
	..("L:[listeners.len]|S:[sounds.len]")

/datum/subsystem/sound/Initialize(timeofday)
	..()
	listeners = list()
	sounds = list()

/datum/subsystem/sound/fire(resumed = FALSE)
	var/list/currentrun = sources.Copy()
	for(var/datum/soundsource/ss in currentrun)
		// Don't fuck with crazy shit
		if (!ss || ss.gcDestroyed || ss.disposed)
			continue

		// If we're set to play once, we're playing, and we're past expected duration...
		if(ss.play_once && ss.started_at && ss.duration <= (world.realtime - ss.started_at))
			// We die and clean up after ourselves
			sources -= ss
			qdel(ss)

		if (MC_TICK_CHECK)
			return
/**
 * Creates a sound source.
 * duration is important.  Other players will not hear the sound after duration, since it'll be deleted server-side.
 * IF YOU PASS A REMOTE URL OR DATA, YOU WILL NEED TO SPECIFY DURATION MANUALLY!
 */
/datum/subsystem/sound/createSourceAtCoords(url, var/area/a, x, y, z, duration=0, z_offset=0, gain=1, vary_rate=TRUE, min_rate=SOUNDSPESS_MIN_RATE, max_rate=SOUNDSPESS_MAX_RATE)
	if (!duration)
		duration = get_sound_duration(url)
		switch (duration)
			if LIBSOUND_ERR_FILE_NOT_FOUND:
				world.log << "Could not open [url] to determine duration!"
				return null
			if LIBSOUND_ERR_DECODING:
				world.log << "File [url] is invalid or corrupt!"
				return null


	if (istype(url, /sound))
		url = sound2base64(url)

	// No not that SS. Calm down, hitler
	var/datum/soundsource/ss = new /datum/soundsource()
	ss.id = src.next_sound_id++
	ss.url = url
	ss.area = a
	ss.x = x
	ss.y = y
	ss._zlevel = z
	ss.z = z_offset
	ss.duration = duration
	ss.min_rate = min_rate
	ss.max_rate = max_rate
	ss.vary = vary_rate
	sounds += ss
	return ss

/**
 * Same as above, but slightly easier to use if you have a /turf available.
 */
/datum/subsystem/sound/createSourceAtTurf(url, var/turf/T, z_offset=0, gain=1, vary_rate=TRUE, min_rate=SOUNDSPESS_MIN_RATE, max_rate=SOUNDSPESS_MAX_RATE)
	return src.createSourceAtCoords(url, get_area(T), T.x, T.y, T.z, z_offset, gain, vary_rate, min_rate, max_rate)

/**
 * Play a sound once at a given turf. Quick and dirty version of playSourceOnceAt() + createSourceAtTurf()
 */
/datum/subsystem/sound/playOnceAt(url, var/turf/T)
	// We're not using createSourceAtTurf because we need the turf and area anyway.
	var/area/a = get_area(T)
	var/datum/soundsource/ss = src.createSourceAtCoords(url, a, T.x, T.y, T.z)
	if(ss)
		for(var/datum/soundlistener/listener in listeners)
			if(ss._zlevel == T.z)
				listener.playOnce(ss)
	return ss
/**
 * Play a sound source once at a given turf. It's deleted client-side after playing.
 *
 * Keep the ref if it's a moving source, because you can still send movement updates!
 */
/datum/subsystem/sound/playSourceOnceAt(var/datum/soundsource/ss, var/turf/T)
	var/area/a = get_area(T)
	for(var/datum/soundlistener/listener in listeners)
		if(ss._zlevel == T.z)
			listener.playOnce(ss)
/**
 * Add a permanent sound source to a given turf. The source can then be played, paused, and looped later.
 */
/datum/subsystem/sound/addSourceAt(var/datum/soundsource/ss, var/turf/T)
	var/area/a = get_area(T)
	for(var/datum/soundlistener/listener in listeners)
		if(ss._zlevel == T.z)
			listener.addSource(ss)
