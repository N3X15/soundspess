# libsoundspess


This literally provides one function which determines the length of a sound file. However,
in order to do that, it needs a fuckload of libraries.

Thankfully, they appear to all be pure Rust, so you won't need to compile ffmpeg and stuff on Windows.

## Installation

Buckle up motherfucker

First, you need rust nightly for i686 installed.

### Ubuntu

```
$ sudo apt install gcc-multilib linux-libc-dev:i386
```
<!--You may also need `libasound2-dev:i386`.-->

(If you're on another distro, you're going to have to figure out the ld errors yourself.  Sorry.)

### Windows
1. Download the Rustup installer
1. Open it
1. **Press `2` to customize.**
1. Enter `i686-pc-windows-msvc` as your triple. **If you fuck up this step, you will build as x64, which won't work!**
1. Finish the rest of the install.

If you messed up by this point, open a commandline prompt and run `rustup default i686-nightly` to fix.

## Building the library
In case you don't want to use BUILD.py in the root directory, for whatever fucking reason:
1. `cargo build --release --target=<whatever your triple is supposed to be>`
2. Copy libsoundspess to the root of your SS13 directory.
   * Linux: `cp target/i686-unknown-linux-gnu/release/liblibsoundspess.so $SS13_DIR/libsoundspess.so` (sic)
   * Windows: `copy target/i686-pc-windows-msvc/release/libsoundspess.dll %SS13_DIR%/libsoundspess.dll`
3. Use src/dreammaker/modules/soundspess/lib.dm or similar to implement the `call()()` interfaces.

## License

libSoundSpess is licensed to you under the MIT Open Source License.

## Exported Functions

### get_sound_duration(filename: String) -> f32
Get the sound duration in deciseconds (1/10ths of a second).

**NOTE:** Accuracy will likely suffer a wee bit since the float needs to be converted to string and back.

#### Example

```dm
#define LIBSOUNDSPESS(fnc,arguments...) call("./libsoundspess.[world.system_type == UNIX ? "so" : "dll"]", fnc)(arguments)
/proc/get_sound_duration(var/soundfile)
	return text2num(LIBSOUNDSPESS("get_sound_duration", soundfile))
/world/New()
	for(var/soundfile in list("sound/item/bikehorn.ogg"))
		world.log << "[soundfile] duration: [get_sound_duration(soundfile)/10]s"
```

#### Error Codes
<table>
<thead><th>Return value</th><th>ErrType variant</th><th>Meaning</th></thead>
<tbody>
<tr><th><code>"-1"</code></th><td><code>ReadError</code></td><td>File open error of some sort (probably filename wrong)</td></tr>
<tr><th><code>"-2"</code></th><td><code>UnreadableFormat</code></td><td>The underlying metadata library choked for some reason.</td></tr>
</tbody>
</table>

#### Support
<table>
<thead><th>File Format</th><th>Library</th><th>File extension</th><th>Encoding/Format</th><th>Supported</th><th>Notes</th></thead>
<tbody>
<tr><th rowspan="5">WAVE</th><th rowspan="5"><a href="https://crates.io/crates/hound">hound</a></th><th rowspan="5"><ul><li>.wav</li></ul></th><th>PCMWAVEFORMAT</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th>WAVEFORMATEX</th><td style="color: green; text-align: center">&#x2713;</td><td>Read-only</td></tr>
<tr><th>WAVEFORMATEXTENSIBLE</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th>WAVE_FORMAT_IEEE_FLOAT</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th>WAVE_FORMAT_INT</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th rowspan="5">Ogg</th><th rowspan="5"><a href="https://crates.io/crates/ogg_metadata">ogg_metadata</a></th><th rowspan="5"><ul><li>.ogg</li><li>.oga</li></ul></th><th>Vorbis</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th>Opus</th><td style="color: green; text-align: center">&#x2713;</td><td>&nbsp;</td></tr>
<tr><th>Theora</th><td style="color: red; text-align: center">&#x2717;</td><td>Ignored, since it's video</td></tr>
<tr><th>Speex</th><td style="color: red; text-align: center">&#x2717;</td><td>&nbsp;</td></tr>
<tr><th>Skeleton</th><td style="color: red; text-align: center">&#x2717;</td><td>&nbsp;</td></tr>
</tbody></table>
