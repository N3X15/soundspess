use std::fs::File;
//use std::io::BufReader;
use std::time::Duration;
use std::path::Path;
use std::ffi::OsStr;

use hound;
use ogg_metadata;
use ogg_metadata::AudioMetadata;

enum ErrType {
	ReadError,
	UnreadableFormat
}

fn get_extension_from_filename(filename: &str) -> Option<&str> {
    Path::new(filename)
        .extension()
        .and_then(OsStr::to_str)
}

// Because rodio is totally fucked in regards to total_duration, I have to use 3 fucking libraries.

// WAVs are handled by Hound, which has a nice, simple API.
fn get_wav_duration(filename: String) -> Result<Duration, ErrType> {
	let rdr = match hound::WavReader::open(filename) {
		Ok(r) => r,
		Err(hound::Error::IoError(_)) => return Err(ErrType::ReadError),
		Err(hound::Error::FormatError(_)) => return Err(ErrType::UnreadableFormat),
		Err(_) => return Err(ErrType::UnreadableFormat),
	};
	// See? Nice and easy.
	Ok(Duration::new((rdr.duration()/rdr.spec().sample_rate).into(), 0))
}

// OGGs are handled by ogg_metadata which is mostly undocumented but readable.
// Oh my fuck this was a pain
fn get_ogg_duration(filename: String) -> Result<Duration, ErrType> {
	let file = match File::open(filename) {
		Ok(f) => f,
		Err(_e) => return Err(ErrType::ReadError),
	};
	let fmts = match ogg_metadata::read_format(file) {
		Ok(f) => f,
		Err(ogg_metadata::OggMetadataError::UnrecognizedFormat) => return Err(ErrType::UnreadableFormat),
		Err(ogg_metadata::OggMetadataError::ReadError(_)) => return Err(ErrType::ReadError),
	};
	// OGGs can have multiple streams, so we have to sum them all together.
	//fmts.map(|fmt: ogg_metadata::OggFormat| fmt.get_duration()).sum()
	let mut duration = Duration::new(0, 0);
	for efmt in fmts {
		// WHY WOULD YOU DO THIS
		// JUST USE A RETURN LIKE EVERYONE ELSE JFC
		match efmt {
			ogg_metadata::OggFormat::Vorbis(s) => {
				duration += s.get_duration().unwrap();
			},
			ogg_metadata::OggFormat::Opus(s) => {
				duration += s.get_duration().unwrap();
			},
			_ => {},
			// Theora is video, so we don't fuck with it.
			// Speex isn't supported yet
			// Skeleton isn't supported yet
		};
	}
	Ok(duration)
}

// Get the sound duration as deciseconds.
//
// # Example:
//
// ```
// #define LIBSOUNDSPESS call("./libsoundspess.[world.system_type == UNIX ? "so" : "dll"]", function)(arguments)
// /proc/get_sound_duration(var/soundfile)
//   return text2num(LIBSOUNDSPESS("get_sound_duration", soundfile))
// /world/New()
//   world.log "sound/item/bikehorn.ogg duration: [get]"
// ```
//
// # Errors:
//  -1 - File open error of some sort (probably filename wrong)
//  -2 - Decoding error
byond!(get_sound_duration: filename; {
	let f = filename.to_string();
	let res = match get_extension_from_filename(filename) {
		Some("wav") => get_wav_duration(f),
		Some("ogg") => get_ogg_duration(f),
		None | Some(_) => return "-1".to_string()
	};
	let duration = match res {
		Ok(v) => v,
		Err(ErrType::ReadError)        => return "-1".to_string(),
		Err(ErrType::UnreadableFormat) => return "-2".to_string(),
	};
	/*
	let file = match File::open(filename) {
		Ok(f) => f,
		Err(_e) => return "-1".to_string(),
	};
	let source = match Decoder::new(BufReader::new(file)) {
		Ok(src) => src,
		Err(_e) => return "-2".to_string(),
	};
	let milliseconds = match source.total_duration() {
		Some(duration) => duration.as_millis(),
		None => return "INFINITY".to_string(), // Infinity?
	};
	*/

	((duration.as_millis() as f32)/100f32).to_string()
});
