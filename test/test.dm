#define LIBSOUNDSPESS(fnc,arguments...) call("./libsoundspess.[world.system_type == UNIX ? "so" : "dll"]", fnc)(arguments)
/proc/get_sound_duration(var/soundfile)
	return text2num(LIBSOUNDSPESS("get_sound_duration", soundfile))
/world/New()
	for(var/soundfile in list("sound/item/bikehorn.ogg"))
		world.log << "[soundfile] duration: [get_sound_duration(soundfile)/10]s"
