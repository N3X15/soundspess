# SoundSpess
*A positional audio system for Space Station 13.*

SoundSpess is a positional audio library making use of modern HTML5 Audio libraries
to provide an immersive 3D sound environment for SS13, without needing ActiveX plugins or Flash.

While originally targeted for /vg/station, this system is designed to be portable to all SS13-based servers, and is
licensed with the MIT Open Source License to ease legal problems. The author is not going to freak out if there's wider adoption.

## Compiling

### Prerequisites
* Python >= 3.6
* Rust
* Python development toolchain
  * Debian/Ubuntu Linux: `sudo apt install python3-dev`
  * Windows: [Consult this nightmare of a chart](https://wiki.python.org/moin/WindowsCompilers)

```shell
# Install vnm, N3X15's experimental Python package manager.
pip install vnm
# Install and set up your virtual env (Add --dev if you want to hack on libsoundspess)
vnm install
# Activate virtualenv for the current console session.
vnm activate
```

### The Crime
```shell
python3 BUILD.py
```

This will build libsoundspess, then generate the JS, minify it, and finally bundle it into an HTML package, and then put the distribution into `dist/`.

**Do not share the *.so, it probably won't work on other distros.**
The Windows DLL should be fairly portable, however.

## Installation
**THIS IS NOT READY YET.** It currently compiles the base engine, but server integration is still a work in progress.

1. Copy DM and JS from `dist/` to their appropriate folders in your server code.
2. Ensure JS is loaded.
  * `soundspess.js` and `soundspess.min.js` are the same file, pick *one*.
3. Disable ye olde jukebox JS. This takes over.

## Comparison to BYOND 513's sound upgrades
BYOND 513 added some new routines for querying clientside sound.

This system will completely bypass BYOND and use HTML5's WebAudio framework to use true 3D sounds, and permit advanced techniques
like hi/lo/bandpass filtering, convolution, gain, directional sound emitters, and even equalizers and frequency displays. SoundSpess
exposes many of these features in its server-side DM scripts.

BYOND is not likely to ever support any of this.

### WHADDABOUT SOUNDQUERY

SoundQuery is for clientside audio and isn't helpful when the *server* needs to know something like how long it should give a sound to play on clients before playing the next sound.

I ran into this with Jukeboxes when I made them, and ended up embedding the duration in the metadata files.

## DM API

### /datum/subsystem/sound
Sound subsystem designed for /vg/'s subsystem thing. All sounds are created, stored, and removed via simple methods on this.

### /datum/soundspess
Overall sound controller for a given z-level. Should be instantiated in world for each z-level.

### /datum/soundsource
A source of sound in the soundspace. Sounds can loop, be muffled, be amplified (or attenuated), and more.

This particular datum represents an Audio tag, a GainNode, a biquad filter, and a PannerNode, all at once. Various nodes can be switched on or off.

### /datum/soundlistener
A representation of a player in the world. Tells the client player the position of the player within the soundspace, as well as provide direct interfaces for the javascript client.

## libsoundspess API
See [libsoundspess/README.md].
