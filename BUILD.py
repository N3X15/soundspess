import os, jinja2
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFilesTarget, CopyFileTarget
from buildtools.maestro.coffeescript import CoffeeBuildTarget
from buildtools.maestro.web import UglifyJSTarget
from buildtools.maestro.package_managers import YarnBuildTarget
from buildtools.maestro.shell import CommandBuildTarget

class JinjaBuildTarget(SingleBuildTarget):
    BT_LABEL = 'JINJA'

    def __init__(self, target, filename, variables={}, filevars={}, dependencies=[]):
        self.variables = variables
        self.filevars = filevars
        super().__init__(target, files=[filename], dependencies=dependencies)


    def build(self):
        for k, filename in self.filevars.items():
            with open(filename, 'r') as f:
                self.variables[k] = f.read()
        jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
        tmpl = jenv.get_template(self.files[0])
        os_utils.ensureDirExists(os.path.dirname(self.target), noisy=True)
        with open(self.target, 'w') as f:
            f.write(tmpl.render(**self.variables))

DIST_DIR = os.path.join('dist')
SRC_DIR = os.path.join('src')
TMP_DIR = os.path.join('tmp')
LIBSND_DIR = os.path.join('libsoundspess')
TEST_DIR = os.path.join('test')
COFFEE_SRC_DIR = os.path.join(SRC_DIR, 'coffee')

env = os_utils.ENV.clone()
env.prependTo('PATH', os.path.join('node_modules', '.bin'), delim=os.pathsep)

CMD = '.cmd' if os_utils.is_windows() else ''
COFFEE = os.path.join('node_modules', '.bin', 'coffee'+CMD)
UGLIFYJS = os.path.join('node_modules', '.bin', 'uglifyjs'+CMD)

bm = BuildMaestro()
argp = bm.build_argparser()
argp.add_argument('--install-to', type=str, default=None, help="Where to install code/, html/, etc to.")
yarn = bm.add(YarnBuildTarget())
coffee = [bm.add(CoffeeBuildTarget(target=os.path.join(TMP_DIR, 'js', 'soundspess.js'), files=[
    # Order matters.
    os.path.join(COFFEE_SRC_DIR, 'Helpers.coffee'),
    os.path.join(COFFEE_SRC_DIR, 'SoundSource.coffee'),
    os.path.join(COFFEE_SRC_DIR, 'SoundSpess.coffee'),
    os.path.join(COFFEE_SRC_DIR, 'BYONDAPI.coffee'),
], dependencies=[yarn.target], coffee_executable=COFFEE)).target]

uglified = bm.add(UglifyJSTarget(target=os.path.join(TMP_DIR, 'js', 'soundspess.min.js'),
                                 inputfile=coffee[0],
                                 uglify_executable=UGLIFYJS,
                                 options=['--comments']))

html = bm.add(JinjaBuildTarget(target=os.path.join(DIST_DIR, 'html', 'soundspess.html'),
                               filename='/'.join([SRC_DIR, 'html', 'soundspess.tmpl.html']),
                               filevars={'js':uglified.target},
                               dependencies=[uglified.target]))

dmmodule = bm.add(CopyFilesTarget(os.path.join(TMP_DIR, 'dm-module.tgt'),
                                  os.path.join(SRC_DIR, 'dreammaker', 'modules', 'soundspess'),
                                  os.path.join(DIST_DIR, 'code', 'modules', 'soundspess')))

dmsubsys = bm.add(CopyFilesTarget(os.path.join(TMP_DIR, 'dm-subsys.tgt'),
                                  os.path.join(SRC_DIR, 'dreammaker', 'controllers'),
                                  os.path.join(DIST_DIR, 'code', 'controllers')))

# Stupid bug on linux, have to do some juggling pre-configure.
triple = 'i686-unknown-linux-gnu'
expected_library = os.path.join(LIBSND_DIR, 'target', triple, 'release', 'liblibsoundspess.so') # Yes, two libs.  That's the stupid bug.
if os_utils.is_windows():
    triple = 'i686-pc-windows-msvc'
    expected_library = os.path.join(LIBSND_DIR, 'target', triple, 'release', 'libsoundspess.dll')
cargo = bm.add(CommandBuildTarget(targets=[expected_library],
                                  files=[os.path.join(LIBSND_DIR, 'src', 'Cargo.toml')]+os_utils.get_file_list(os.path.join(LIBSND_DIR, 'src'), prefix=os.path.join(LIBSND_DIR, 'src')),
                                  cmd=['cargo', 'build', '--release', '--target='+triple],
                                  show_output=True,
                                  cwd=LIBSND_DIR))

libsnd = bm.add(CopyFileTarget(target=os.path.join(DIST_DIR, 'libsoundspess.so' if os_utils.is_linux() else 'libsoundspess.dll'), filename=expected_library, verbose=True))
bm.add(CopyFileTarget(target=os.path.join(TEST_DIR, 'libsoundspess.so' if os_utils.is_linux() else 'libsoundspess.dll'), filename=expected_library, verbose=True))

bm.other_dirs_to_clean += [DIST_DIR, TMP_DIR, os.path.join(LIBSND_DIR, 'target')]

args = bm.parse_args(argp)

if args.install_to is not None:
    bm.add(CopyFilesTarget(target=os.path.join(TMP_DIR, 'install.tgt'),
                           source=os.path.join(DIST_DIR),
                           destination=args.install_to,
                           dependencies=[dmsubsys.target, dmmodule.target, html.target, libsnd.target]))
bm.as_app(argp)
